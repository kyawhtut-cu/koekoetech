package com.kyawhtut.koekoetechcodingtest.util.sync

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.core.app.JobIntentService
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import coil.Coil
import coil.api.get
import com.kyawhtut.koekoetechcodingtest.R
import com.kyawhtut.koekoetechcodingtest.util.AppConstant
import timber.log.Timber
import java.util.*

class ScheduledService : JobIntentService() {

    override fun onHandleWork(intent: Intent) {
        val notification = NotificationUtils(this).setNotificationChannel(
                AppConstant.NOTIFICATION_ID,
                AppConstant.NOTIFICATION_CHANNEL
        )

        val builder = NotificationCompat.Builder(this, AppConstant.NOTIFICATION_ID).apply {
            setSmallIcon(R.mipmap.ic_launcher)
            setLargeIcon(BitmapFactory.decodeResource(this@ScheduledService.resources,
                    R.mipmap.ic_launcher))
            color = ContextCompat.getColor(this@ScheduledService, R.color.colorPrimary)
            setContentTitle("${Calendar.getInstance().time}")
            setContentText("Message")
            setAutoCancel(false)
        }

        notification.notificationManager.notify(System.currentTimeMillis().toInt(), builder.build())
        notification.playNotificationSound()
    }

    companion object {
        private const val UNIQUE_JOB_ID = 1337

        internal fun enqueueWork(ctx: Context) {
            enqueueWork(ctx, ScheduledService::class.java, UNIQUE_JOB_ID,
                    Intent(ctx, ScheduledService::class.java))
        }
    }
}