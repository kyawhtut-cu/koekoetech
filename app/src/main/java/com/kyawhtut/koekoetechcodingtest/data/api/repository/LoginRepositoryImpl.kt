package com.kyawhtut.koekoetechcodingtest.data.api.repository

import android.content.SharedPreferences
import com.kyawhtut.koekoetechcodingtest.data.state.LoginViewState
import com.kyawhtut.koekoetechcodingtest.util.AppConstant
import com.kyawhtut.koekoetechcodingtest.util.put
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.delay

class LoginRepositoryImpl(private val sh: SharedPreferences) : LoginRepository {

    override suspend fun saveLogin(phoneNo: String): LoginViewState {
        val response = CompletableDeferred<LoginViewState>()
        delay(5000)
        if (phoneNo == "09973419006") {
            sh.put(AppConstant.KEY_LOGIN_PHONE, phoneNo)
            response.complete(LoginViewState.Success("Login Success"))
        } else {
            response.complete(LoginViewState.LoginFail("Login Fail"))
        }
        return response.await()
    }
}