package com.kyawhtut.koekoetechcodingtest.data.api.response

import com.google.gson.annotations.SerializedName
import com.kyawhtut.koekoetechcodingtest.data.vos.ClinicVO

data class ClinicResponse(
        @SerializedName("TotalCount")
        val totalCount: Int,
        @SerializedName("TotalPage")
        val totalPage: Int,
        val prevLink: String,
        val nextLink: String,
        @SerializedName("Results")
        val results: MutableList<ClinicVO>
)