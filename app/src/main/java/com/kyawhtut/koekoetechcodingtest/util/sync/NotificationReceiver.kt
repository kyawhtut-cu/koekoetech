package com.kyawhtut.koekoetechcodingtest.util.sync

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import timber.log.Timber

class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == null) {
            ScheduledService.enqueueWork(context)
        }
    }

    companion object {
        fun scheduleAlarms(cxt: Context) {
            val mgr = cxt.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val i = Intent(cxt, NotificationReceiver::class.java)
            val pi = PendingIntent.getBroadcast(cxt, 0, i, 0)
            mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 1000 * 60 * 5,
                    1000, pi)
        }
    }
}