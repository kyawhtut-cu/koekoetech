package com.kyawhtut.koekoetechcodingtest.data.api.injection

import com.kyawhtut.koekoetechcodingtest.BuildConfig
import com.kyawhtut.koekoetechcodingtest.data.api.service.ClinicService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ServiceInjector {

    private fun provideRetrofit(baseUrl: String, internetConnectivityInterceptor: InternetConnectivityInterceptor): Retrofit {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.writeTimeout(60, TimeUnit.SECONDS)
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.addInterceptor(internetConnectivityInterceptor)

        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.networkInterceptors().add(httpLoggingInterceptor)
        }

        val okHttpClient = builder.build()

        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
    }

    fun provideClinicService(baseUrl: String, internetConnectivityInterceptor: InternetConnectivityInterceptor): ClinicService {
        return provideRetrofit(baseUrl, internetConnectivityInterceptor).create(ClinicService::class.java)
    }
}