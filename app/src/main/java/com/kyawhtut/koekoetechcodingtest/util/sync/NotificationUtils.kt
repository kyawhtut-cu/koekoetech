package com.kyawhtut.koekoetechcodingtest.util.sync

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.provider.Settings
import coil.Coil
import coil.api.get
import coil.api.load
import kotlinx.coroutines.CompletableDeferred

class NotificationUtils constructor(private val ctx: Context) {

    val notificationManager: NotificationManager
        get() = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    fun setNotificationChannel(channelId: String, channelName: String): NotificationUtils {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel =
                    NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH).apply {
                        enableLights(true)
                        lightColor = Color.RED
                        lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                        setSound(
                                Settings.System.DEFAULT_NOTIFICATION_URI, AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                                .build()
                        )
                        enableVibration(true)
                        vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                    }
            notificationManager.createNotificationChannel(notificationChannel)
        }
        return this
    }

    fun playNotificationSound() {
        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        try {
            RingtoneManager.getRingtone(ctx, uri).play()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}