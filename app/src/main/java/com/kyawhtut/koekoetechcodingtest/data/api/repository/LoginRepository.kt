package com.kyawhtut.koekoetechcodingtest.data.api.repository

import com.kyawhtut.koekoetechcodingtest.data.state.LoginViewState

interface LoginRepository {

    suspend fun saveLogin(phoneNo: String): LoginViewState
}