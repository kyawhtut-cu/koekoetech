package com.kyawhtut.koekoetechcodingtest.activity

import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.kyawhtut.koekoetechcodingtest.R
import com.kyawhtut.koekoetechcodingtest.components.LoadingViewPod
import com.kyawhtut.koekoetechcodingtest.data.model.LoginViewModel
import com.kyawhtut.koekoetechcodingtest.data.state.LoginViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_phone_login.*
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class PhoneNoLoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()
    private val disposable: CompositeDisposable by inject()
    private var dialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_login)

        ed_phone.addTextChangedListener(
                onTextChanged = { _, _, _, _ ->
                    viewModel.phoneNumber = ed_phone.text.toString()
                    btn_login.isEnabled = viewModel.isEnable
                }
        )

        btn_login.setOnClickListener { viewModel.saveUser() }
    }

    private fun startStateSubscription() {
        viewModel.state.observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = {
                    renderState(it)
                },
                onError = {
                    Timber.e(it)
                }
        ).addTo(disposable)
    }

    private fun renderState(it: LoginViewState) {
        when (it) {
            is LoginViewState.Loading -> renderLoadingState()
            is LoginViewState.Success -> renderSuccessState(it.data)
            is LoginViewState.LoginFail -> renderFailState(it.data)
        }
    }

    private fun renderLoadingState() {
        if (dialog == null) {
            val loadingView = LoadingViewPod(this@PhoneNoLoginActivity)
            loadingView.loadingMessage = "Please wait..."
            loadingView.isLoading = true

            dialog = MaterialDialog(this).apply {
                setCancelable(false)
                cornerRadius(16f)
            }.customView(view = loadingView)
        }

        dialog!!.show()
    }

    private fun renderSuccessState(data: String) {
        MaterialDialog(this).show {
            title(text = data)
            message(text = "Phone number correct.")
            cornerRadius(16f)
            setCancelable(false)
            positiveButton(text = "OK") {
                it.dismiss()
                finish()
                startActivity<HomeActivity>()
            }
        }
        dialog!!.dismiss()
    }

    private fun renderFailState(data: String) {
        MaterialDialog(this).show {
            title(text = data)
            message(text = "Phone number incorrect.")
            cornerRadius(16f)
            positiveButton(text = "OK")
        }
        dialog!!.dismiss()
    }

    override fun onResume() {
        super.onResume()
        startStateSubscription()
    }

    override fun onPause() {
        super.onPause()
        disposable.clear()
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}