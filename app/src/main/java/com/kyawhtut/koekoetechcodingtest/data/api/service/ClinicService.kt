package com.kyawhtut.koekoetechcodingtest.data.api.service

import com.kyawhtut.koekoetechcodingtest.data.api.response.ClinicResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ClinicService {

    @GET("api/clinic/getall")
    suspend fun getClinicByPage(
            @Query("page") page: Int
    ): ClinicResponse
}