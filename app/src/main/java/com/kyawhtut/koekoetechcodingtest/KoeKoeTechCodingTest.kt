package com.kyawhtut.koekoetechcodingtest

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import coil.Coil
import coil.util.CoilLogger
import com.kyawhtut.koekoetechcodingtest.di.*
import com.kyawhtut.koekoetechcodingtest.util.buildDefaultImageLoader
import com.kyawhtut.koekoetechcodingtest.util.sync.NotificationReceiver
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.core.context.startKoin
import timber.log.Timber

class KoeKoeTechCodingTest : Application() {

    override fun onCreate() {
        super.onCreate()

        CoilLogger.setEnabled(true)
        Coil.setDefaultImageLoader(buildDefaultImageLoader())

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        startKoin {
            androidContext(applicationContext)
            androidFileProperties()
            modules(
                    listOf(
                            appModule,
                            preferenceModule,
                            serviceModule,
                            repositoryModule,
                            viewModelModule
                    )
            )
        }

        NotificationReceiver.scheduleAlarms(this)
    }
}