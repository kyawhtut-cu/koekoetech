package com.kyawhtut.koekoetechcodingtest.util

import android.app.Application
import coil.ImageLoader

fun Application.buildDefaultImageLoader(): ImageLoader {
    return ImageLoader(this) {
        availableMemoryPercentage(0.5)
        bitmapPoolPercentage(0.5)
    }
}