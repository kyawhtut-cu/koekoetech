package com.kyawhtut.koekoetechcodingtest.data.api.repository

import com.kyawhtut.koekoetechcodingtest.data.state.HomeViewState

interface HomeRepository {
    suspend fun getClinicByPage(page: Int): HomeViewState
}