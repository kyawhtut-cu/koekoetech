package com.kyawhtut.koekoetechcodingtest.activity

import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.kyawhtut.koekoetechcodingtest.R
import com.kyawhtut.koekoetechcodingtest.data.vos.ClinicVO
import com.kyawhtut.koekoetechcodingtest.util.source
import kotlinx.android.synthetic.main.activity_clinic_detail.*
import org.jetbrains.anko.email

class ClinicDetailActivity : AppCompatActivity() {

    private var clinicVO: ClinicVO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clinic_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        clinicVO = intent.getSerializableExtra(extraClinicData) as ClinicVO

        fab.setOnClickListener { view ->
            clinicVO?.clinicEmail?.let {
                email(it, "KoeKoeTech Coding Test")
            }
        }

        clinicVO?.let {
            iv_clinic.source(it.getClinicPhotoUrl())
            title = it.clinicName
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val extraClinicData = "extra.clinicData"
    }
}
