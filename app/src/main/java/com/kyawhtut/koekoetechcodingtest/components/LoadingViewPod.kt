package com.kyawhtut.koekoetechcodingtest.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.kyawhtut.koekoetechcodingtest.R
import com.kyawhtut.koekoetechcodingtest.util.inflate
import kotlinx.android.synthetic.main.view_pod_loading.view.*

class LoadingViewPod : FrameLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        addView(context.inflate(R.layout.view_pod_loading, this, false))
        init(context, attrs, defStyleAttr)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.LoadingViewPod, defStyleAttr, 0)
        try {
            loadingMessage = a.getString(R.styleable.LoadingViewPod_loading_message) ?: "Loading..."
            isLoading = a.getBoolean(R.styleable.LoadingViewPod_isLoading, false)
        } finally {
            a.recycle()
        }
    }

    var loadingMessage: String = "Loading..."
        set(value) {
            field = value
            tv_loading_message.text = value
        }

    var isLoading: Boolean = false
        set(value) {
            field = value
            visibility = if (value) View.VISIBLE else View.GONE
        }
}