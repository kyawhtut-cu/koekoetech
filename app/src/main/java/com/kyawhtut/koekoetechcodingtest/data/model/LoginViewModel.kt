package com.kyawhtut.koekoetechcodingtest.data.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kyawhtut.koekoetechcodingtest.data.api.repository.LoginRepositoryImpl
import com.kyawhtut.koekoetechcodingtest.data.state.LoginViewState
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class LoginViewModel(private val repository: LoginRepositoryImpl) : ViewModel() {

    val state: PublishSubject<LoginViewState> = PublishSubject.create()

    var phoneNumber: String = ""
    val isEnable: Boolean
        get() = phoneNumber.isNotEmpty()

    fun saveUser() {
        viewModelScope.launch(Dispatchers.IO) {
            state.onNext(LoginViewState.Loading)
            state.onNext(repository.saveLogin(phoneNumber))
        }
    }
}