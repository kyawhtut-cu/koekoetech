package com.kyawhtut.koekoetechcodingtest.data.state

sealed class LoginViewState {

    object Loading : LoginViewState()
    data class Success(var data: String) : LoginViewState()
    data class LoginFail(var data: String) : LoginViewState()
}