package com.kyawhtut.koekoetechcodingtest.data.vos

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ClinicVO(
        @SerializedName("ClinicId")
        val clinicId: Int,
        @SerializedName("Name")
        val clinicName: String,
        @SerializedName("Email")
        val clinicEmail: String? = null,
        @SerializedName("Phone")
        val clinicPhone: String,
        @SerializedName("AddressMyanmar")
        val clinicAddressMyanmar: String,
        @SerializedName("AddressEnglish")
        val clinicAddressEnglisth: String,
        @SerializedName("Township")
        val clinicTownship: String,
        @SerializedName("StateDivision")
        val clinicStateDivision: String,
        @SerializedName("Website")
        val clinicWebSite: String? = null,
        @SerializedName("Specialty")
        val clinicSpecialty: String? = null,
        @SerializedName("IsDeleted")
        val clinicIsDeleted: Boolean? = null,
        @SerializedName("Accesstime")
        val clinicAccessTime: String,
        @SerializedName("Active")
        val clinicActive: Boolean,
        @SerializedName("Photo")
        val clinicPhoto: String? = null,
        @SerializedName("PostedUser")
        val clinicPostedUser: Int? = null,
        @SerializedName("Lat")
        val clinicLat: String,
        @SerializedName("Lng")
        val clinicLng: String,
        @SerializedName("Priority")
        val clinicPriority: Int
) : Serializable {

    //d0a717f1-1670-4fca-b541-95b51053087d.jpg
    fun getClinicPhotoUrl(): String {
        return "http://portalvhdslvb28rs1c3tmc.blob.core.windows.net/maymay/clinic/$clinicPhoto"
    }
}