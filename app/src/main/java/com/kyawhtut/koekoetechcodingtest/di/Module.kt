package com.kyawhtut.koekoetechcodingtest.di

import com.kyawhtut.koekoetechcodingtest.data.api.injection.InternetConnectivityInterceptor
import com.kyawhtut.koekoetechcodingtest.data.api.injection.PreferenceInjector
import com.kyawhtut.koekoetechcodingtest.data.api.injection.ServiceInjector
import com.kyawhtut.koekoetechcodingtest.data.api.repository.HomeRepositoryImpl
import com.kyawhtut.koekoetechcodingtest.data.api.repository.LoginRepositoryImpl
import com.kyawhtut.koekoetechcodingtest.data.api.service.ClinicService
import com.kyawhtut.koekoetechcodingtest.data.model.HomeViewModel
import com.kyawhtut.koekoetechcodingtest.data.model.LoginViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    single {
        InternetConnectivityInterceptor(get())
    }

    single {
        CompositeDisposable()
    }
}

val preferenceModule = module {
    single(named("font")) {
        PreferenceInjector().provideFontSharedPreference(get())
    }

    single(named("default")) {
        PreferenceInjector().provideDefaultSharedPreference(get())
    }
}

val serviceModule = module {

    single {
        ServiceInjector().provideClinicService(getProperty("BASE_URL"), get())
    }
}

val repositoryModule = module {

    single {
        HomeRepositoryImpl(get())
    }

    single {
        LoginRepositoryImpl(get(named("default")))
    }
}

val viewModelModule = module {

    viewModel {
        HomeViewModel(get(), get(named("default")))
    }

    viewModel {
        LoginViewModel(get())
    }
}