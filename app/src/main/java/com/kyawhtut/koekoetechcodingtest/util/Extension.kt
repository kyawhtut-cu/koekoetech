package com.kyawhtut.koekoetechcodingtest.util

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import coil.api.loadAny
import coil.transform.CircleCropTransformation
import com.kyawhtut.koekoetechcodingtest.R

fun Context.inflate(layoutId: Int, container: ViewGroup?, attachToRoot: Boolean = false) =
        LayoutInflater.from(this).inflate(layoutId, container, attachToRoot)

fun Context?.getContextDrawable(resource: Int) = ContextCompat.getDrawable(this!!, resource)

fun Activity.getContextDrawable(resource: Int) = applicationContext.getContextDrawable(resource)

fun Fragment.getContextDrawable(resource: Int) = context.getContextDrawable(resource)

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun ImageView.source(
        resource: Any,
        error: Int = R.drawable.thumbnail_image,
        placeholder: Int = R.drawable.thumbnail_image
) {
    this.loadAny(resource) {
        crossfade(true)
        error(error)
        placeholder(placeholder)
    }
}

fun ImageView.circleSource(
        resource: Any,
        error: Int = R.drawable.thumbnail_image,
        placeholder: Int = R.drawable.thumbnail_image
) {
    this.loadAny(resource) {
        crossfade(true)
        error(error)
        placeholder(placeholder)
        transformations(CircleCropTransformation())
    }
}