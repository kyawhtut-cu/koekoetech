package com.kyawhtut.koekoetechcodingtest.util

object AppConstant {

    const val KEY_LOGIN_PHONE = "KEY_LOGIN_PHONE"

    const val NOTIFICATION_ID = "0x0110"
    const val NOTIFICATION_CHANNEL = "Information Notifcation"
}