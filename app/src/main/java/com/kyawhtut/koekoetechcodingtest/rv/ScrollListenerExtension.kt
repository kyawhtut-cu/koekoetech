package com.kyawhtut.koekoetechcodingtest.rv

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

const val TOP = 0x1
const val END = 0x2
const val BOTH = 0x3

open class ScrollListener(
        private val type: Int,
        private val onLoadMoreTop: () -> Unit = {},
        private val onLoadMoreEnd: () -> Unit = {}
) : RecyclerView.OnScrollListener() {

    private var isListTopReach = false
    private var isListEndReach = false
    private var previousDy = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (previousDy < dy) {
            isListTopReach = false
        } else if (previousDy > dy) {
            isListEndReach = false
        }
        previousDy = dy
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            val firstItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            val visibleItemCount = recyclerView.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            /*Timber.i("onLoadmore Position : $previousDy $isListTopReach $firstItemPosition $visibleItemCount $totalItemCount")*/
            if (firstItemPosition == 0 && !isListTopReach && (type == TOP || type == BOTH)) {
                isListTopReach = true
                /*Timber.i("onLoadmore start reach top of recyclerview $isListTopReach")*/
                onLoadMoreTop()
            } else if ((visibleItemCount + firstItemPosition) >= totalItemCount && !isListEndReach && (type == END || type == BOTH)) {
                isListEndReach = true
                onLoadMoreEnd()
            }
        }
    }
}

fun RecyclerView.onLoadMoreTop(onLoadMoreTop: () -> Unit) {
    this.addOnScrollListener(ScrollListener(TOP, onLoadMoreTop))
}

fun RecyclerView.onLoadMoreEnd(onLoadMoreEnd: () -> Unit) {
    this.addOnScrollListener(ScrollListener(END, onLoadMoreEnd = onLoadMoreEnd))
}

fun RecyclerView.onLoadMore(onLoadMoreTop: () -> Unit, onLoadMoreEnd: () -> Unit) {
    this.addOnScrollListener(ScrollListener(BOTH, onLoadMoreTop, onLoadMoreEnd))
}

fun RecyclerView.overLapItem(space: Int, ignorePosition: MutableList<Int> = mutableListOf(0)) {
    this.addItemDecoration(ItemDecorator(space, ignorePosition))
}

class ItemDecorator(private val space: Int,private val ignorePosition: MutableList<Int>) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        if (!ignorePosition.contains(position))
            outRect.top = space
    }
}