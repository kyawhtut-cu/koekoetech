package com.kyawhtut.koekoetechcodingtest.data.model

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kyawhtut.koekoetechcodingtest.data.api.repository.HomeRepositoryImpl
import com.kyawhtut.koekoetechcodingtest.data.vos.ClinicVO
import com.kyawhtut.koekoetechcodingtest.data.state.HomeViewState
import com.kyawhtut.koekoetechcodingtest.util.AppConstant
import com.kyawhtut.koekoetechcodingtest.util.clear
import com.kyawhtut.koekoetechcodingtest.util.get
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel(private val repository: HomeRepositoryImpl, private val sh: SharedPreferences) : ViewModel() {

    var currentPage: Int = 1
    var totalCount: Int = 0
    var clinicList = mutableListOf<ClinicVO>()

    val state: PublishSubject<HomeViewState> = PublishSubject.create()

    fun getClinicByPage() {
        viewModelScope.launch(Dispatchers.IO) {
            state.onNext(HomeViewState.Loading(currentPage))
            state.onNext(repository.getClinicByPage(currentPage))
        }
    }

    val isHasMoreData: Boolean
        get() = clinicList.size != totalCount

    val isLogin: Boolean
        get() = sh.get(AppConstant.KEY_LOGIN_PHONE, "").isNotEmpty()

    fun logout() {
        sh.clear()
    }
}