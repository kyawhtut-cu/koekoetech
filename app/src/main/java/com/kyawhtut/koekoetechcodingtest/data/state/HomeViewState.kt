package com.kyawhtut.koekoetechcodingtest.data.state

import com.kyawhtut.koekoetechcodingtest.data.api.response.ClinicResponse

sealed class HomeViewState {

    data class NoConnection(var page: Int) : HomeViewState()
    data class Loading(var page: Int) : HomeViewState()
    data class Error(var error: String, var page: Int) : HomeViewState()
    data class Success(var data: ClinicResponse, var page: Int) : HomeViewState()
}