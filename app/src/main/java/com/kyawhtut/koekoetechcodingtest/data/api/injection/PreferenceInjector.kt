package com.kyawhtut.koekoetechcodingtest.data.api.injection

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

class PreferenceInjector {

    fun provideFontSharedPreference(app: Application): SharedPreferences {
        return app.getSharedPreferences("unicdoe_zawgyi_preference", Context.MODE_PRIVATE)
    }

    fun provideDefaultSharedPreference(app: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }
}