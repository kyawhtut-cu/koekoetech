package com.kyawhtut.koekoetechcodingtest.data.api.injection

import java.io.IOException

class NetworkException(message: String): IOException(message)