package com.kyawhtut.koekoetechcodingtest.data.api.repository

import com.kyawhtut.koekoetechcodingtest.data.api.injection.NetworkException
import com.kyawhtut.koekoetechcodingtest.data.api.service.ClinicService
import com.kyawhtut.koekoetechcodingtest.data.state.HomeViewState
import kotlinx.coroutines.CompletableDeferred
import timber.log.Timber

class HomeRepositoryImpl(private val service: ClinicService) : HomeRepository {
    override suspend fun getClinicByPage(page: Int): HomeViewState {
        val response = CompletableDeferred<HomeViewState>()
        try {
            response.complete(HomeViewState.Success(service.getClinicByPage(page), page))
        } catch (e: Exception) {
            Timber.e(e)
            if (e is NetworkException) {
                response.complete(HomeViewState.NoConnection(page))
            } else {
                response.complete(HomeViewState.Error(e.localizedMessage
                        ?: "Connection time out", page))
            }
        }
        return response.await()
    }
}