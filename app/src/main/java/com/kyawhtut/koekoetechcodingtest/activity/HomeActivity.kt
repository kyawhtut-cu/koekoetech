package com.kyawhtut.koekoetechcodingtest.activity

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.kyawhtut.koekoetechcodingtest.R
import com.kyawhtut.koekoetechcodingtest.data.api.response.ClinicResponse
import com.kyawhtut.koekoetechcodingtest.data.model.HomeViewModel
import com.kyawhtut.koekoetechcodingtest.rv.*
import com.kyawhtut.koekoetechcodingtest.data.state.HomeViewState
import com.kyawhtut.koekoetechcodingtest.util.px
import com.kyawhtut.koekoetechcodingtest.util.source
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.list_item_clinic.view.*
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class HomeActivity : AppCompatActivity() {

    private val viewModel: HomeViewModel by viewModel()
    private val disposable: CompositeDisposable by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (!viewModel.isLogin) {
            finish()
            startActivity<PhoneNoLoginActivity>()
        }

        setupRv()

        if (viewModel.currentPage == 1) {
            viewModel.getClinicByPage()
        }

        swipe_refresh.setOnRefreshListener {
            viewModel.currentPage = 1
            viewModel.getClinicByPage()
        }
    }

    private fun subScribeState() {
        viewModel.state.observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = {
                    renderState(it)
                },
                onError = {
                    Timber.e(it)
                }
        ).addTo(disposable)
    }

    private fun setupRv() {
        rv_clinic.apply {
            bind(viewModel.clinicList, R.layout.list_item_clinic) { item, pos ->
                this.iv_clinic.source(item.getClinicPhotoUrl())
                this.tv_clinic_name.text = item.clinicName
                this.setOnClickListener {
                    startActivity<ClinicDetailActivity>(
                            ClinicDetailActivity.extraClinicData to item
                    )
                }
            }.layoutManager(GridLayoutManager(this@HomeActivity, 2))
            addItemDecoration(GridSpacingItemDecoration(2, 8.px, true))
            onLoadMoreEnd {
                if (viewModel.isHasMoreData) {
                    viewModel.getClinicByPage()
                } else {
                    Snackbar.make(rv_clinic, "No more data available.", Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun renderState(it: HomeViewState) {
        when (it) {
            is HomeViewState.NoConnection -> renderNoConnection(it.page)
            is HomeViewState.Loading -> renderLoading(it.page)
            is HomeViewState.Error -> renderError(it.error, it.page)
            is HomeViewState.Success -> renderSuccess(it.data, it.page)
        }
    }

    private fun renderNoConnection(page: Int) {
        hideLoading(page)
        MaterialDialog(this).show {
            icon(res = R.drawable.ic_no_internet_connection)
            title(text = "No internet connection")
            message(text = "Please open the internet.")
            cornerRadius(16f)
            positiveButton(text = "OK")
        }
    }

    private fun renderLoading(page: Int) {
        swipe_refresh.isRefreshing = page == 1
        vp_loading.isLoading = page != 1
        vp_loading.loadingMessage = "Loading Page $page..."
    }

    private fun renderError(error: String, page: Int) {
        Timber.e(error)
        hideLoading(page)
    }

    private fun renderSuccess(data: ClinicResponse, page: Int) {
        hideLoading(page)
        if (data.results.isNotEmpty()) {
            viewModel.currentPage++
            viewModel.totalCount = data.totalCount

            if (page == 1) {
                rv_clinic.update(data.results)
                viewModel.clinicList.clear()
                viewModel.clinicList.addAll(data.results)
            } else {
                rv_clinic.add(data.results)
                data.results.map {
                    viewModel.clinicList.add(it)
                }
            }
        }
    }

    private fun hideLoading(page: Int) {
        if (page == 1) {
            swipe_refresh.isRefreshing = false
        } else {
            vp_loading.isLoading = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_logout -> {
                viewModel.logout()
                finish()
                startActivity<PhoneNoLoginActivity>()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        subScribeState()
    }

    override fun onPause() {
        super.onPause()
        disposable.clear()
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}
